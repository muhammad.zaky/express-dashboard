'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class artikel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      artikel.belongsTo(models.kategori, {foreignKey: 'kategori_id', as: 'kategori'})
    }
  };
  artikel.init({
    judul: DataTypes.STRING,
    kontent: DataTypes.STRING,
    kategori_id: DataTypes.INTEGER,
    view: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'artikel',
  });
  return artikel;
};