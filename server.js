const express = require("express");
const passport = require("passport");
const { artikel, user, kategori } = require("./models");
const auth = require("./middleware/auth");

const app = express();
require("./middleware/passport");

app.use(express.static("public"));
app.set("view engine", "ejs");

app.use(express.urlencoded());
app.use(express.json());

app.use(
  require("express-session")({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());

// Routing
app.get("/", auth, async (req, res) => {
  console.log("ab", req.user);
  const { user } = req;
  const path = req.originalUrl;
  const artikels = await artikel.findAll({
    raw: true,
  });
  const articlesCount = artikels.length;
  return res.render("index", { path, articlesCount, user });
});

app.get("/artikel", auth, async (req, res) => {
  const artikels = await artikel.findAll({
    order: [["id", "DESC"]],
    include: 'kategori',
    raw: true,
  });

  const path = req.originalUrl;
  return res.render("article", { path, artikels });
});

app.get("/artikel/detail/:id", auth, async (req, res) => {
  const { id } = req.params;
  const path = req.originalUrl;

  const artikels = await artikel.findByPk(id);
  res.render("detailartikel", { post: artikels, path });
});

app.get("/artikel/tambah", auth, (req, res) => {
  const path = req.originalUrl;
  res.render("tambaharticle", { path });
});

app.post("/artikel/tambah", auth, async (req, res) => {
  const { judul, kontent } = req.body;

  const insertArticle = await artikel.create({
    judul: judul,
    kontent: kontent,
  });

  if (insertArticle.id) {
    res.redirect("/artikel");
  } else {
    res.send("failed to save data to database");
  }
});

app.get("/artikel/hapus/:id", async (req, res) => {
  const { id } = req.params;

  const deleteArtikel = await artikel.destroy({
    where: {
      id,
    },
  });

  if (deleteArtikel) {
    res.redirect("/artikel");
  } else {
    res.send("gagal delete artikel");
  }
});

app.get("/artikel/edit/:id", async (req, res) => {
  const { id } = req.params;
  const artikels = await artikel.findByPk(id);
  const path = req.originalUrl;
  res.render("editartikel", { artikels, path });
});

app.post("/artikel/edit/:id", async (req, res) => {
  const { id } = req.params;
  const { judul, kontent } = req.body;

  const updateArtikel = await artikel.update(
    {
      judul,
      kontent,
    },
    {
      where: {
        id,
      },
    }
  );

  if(updateArtikel.length){
    res.redirect('/artikel')
  } else {
    res.send("gagal mengubah artikel")
  }

});

app.get('/kategori', async(req, res) => {
  const kategories = await kategori.findAll({
    include: 'artikel'
  });
  console.log(kategories)

  const path = req.originalUrl;

  res.render('kategori', {path, kategories})

})

app.get("/login", (req, res) => {
  return res.render("login", { message: null });
});

app.post(
  "/login",
  passport.authenticate("local", {
    failureRedirect: "/login",
    successRedirect: "/",
  })
);

app.listen(4000, () => {
  console.log("aplikasi berjalan di port 4000");
});
