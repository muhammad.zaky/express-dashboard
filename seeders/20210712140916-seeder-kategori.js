"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "kategoris",
      [
        {
          name: "teknologi",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "sains",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("kategoris", null, {});
  },
};
