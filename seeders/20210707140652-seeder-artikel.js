"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "artikels",
      [
        {
          judul: "Cara Upgrade Ke Windows 11",
          kontent:
            "cara upgrate dasldf adsf asdfads fadf adsfa sdfa dsfa sdfa dsfa sdfa dsf adsf adsf asdf ",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          judul: "Review Windows 11",
          kontent:
            "cara upgrate dasldf adsf asdfads fadf adsfa sdfa dsfa sdfa dsfa sdfa dsf adsf adsf asdf ",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          judul: "Review Windows 10",
          kontent:
            "cara upgrate dasldf adsf asdfads fadf adsfa sdfa dsfa sdfa dsfa sdfa dsf adsf adsf asdf ",
          kategori_id: 1,
          view: 1000,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("artikels", null, {});
  },
};
